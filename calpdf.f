      subroutine calpdf (t, de, dh, recpdf)
c
      include 'cblocks.inc'
c
c           Variables
c
      real*8 t, de, dh, telc
      real*8 xp(mxion, mxelem)
      real*8 energ,rkt,phots
      real*8 wid
      real*8 recpdf(mxinfph),rectot
      real*8 normff(mxinfph),embri(mxinfph)
c
c      integer*4 ie,ii2,jn
      integer i,j,inl,nz
      integer line,series,atom,ion,trans
c
      integer*4 lulin      
c
c
      telc=dmax1(t,1.d-2)
      rkt=rkb*telc
c
      do j=1,atypes
        do i=1,maxion(j)-1
          xp(i,j)=1.0d0
        enddo
      enddo
c
c     ***FIND LINES BRIGHTNESSES
c
      call cool (t, de, dh)
c
      jcon='YES'
c
      call freebound (t, de, dh)
      call freefree (t, de, dh)
      call twophoton (t, de, dh)
c
cc      open (lulin,file='/Users/jyf/Desktop/code_check/recpdf'
cc     &     ,status='UNKNOWN') 
c     
      do inl=1,infph
         energ=cphote(inl)
         wid=evplk*(photev(j+1)-photev(j))         
         phots=ffph(inl)+fbph(inl)+p2ph(inl)
         normff(inl)=0.0d0
         recpdf(inl)=0.0d0 
         embri(inl)=0.0d0        
         normff(inl)=phots*energ*wid
         embri(inl)=normff(inl)
cc         write (lulin,*) lmev/cphotev(inl),normff(inl),'con'
      enddo
c
c     Now add weak lines directly to vector emidif...but not
c     emidifcont
c
c    Multi-level atoms
c    Include [OIII],[OII],[NIV] etc.
c
      do ion=1,nfmions
        do trans=1,nfmtrans(ion)
          j=fmbin(trans,ion)
          if ((j.ne.0).and.(fmbri(trans,ion).gt.epsilon)) then
            energ=fmeij(trans,ion)            
cc            write (lulin,*) plk*cls/energ*1.d8,fmbri(trans,ion),'fmbri'
            embri(j)=embri(j)+fmbri(trans,ion)
          endif
        enddo
      enddo
cc
cc    Fe II-VII lines
c
      do ion=1,nfeions
        do trans=1,nfetrans(ion)
          j=febin(trans,ion)
          if ((j.ne.0).and.(febri(trans,ion).gt.epsilon)) then
            energ=feeij(trans,ion)
cc            write (lulin,*) plk*cls/energ*1.d8,febri(trans,ion),'febri'
            embri(j)=embri(j)+febri(trans,ion)
          endif
        enddo
      enddo
cc
c
c    Three level atoms
c
      do ion=1,nf3ions
        do trans=1,nf3trans
          j=f3bin(trans,ion)
          if ((j.ne.0).and.(f3bri(trans,ion).gt.epsilon)) then
cc            write (lulin,*) f3lam(trans,ion)*1.d8,
cc     &                      f3bri(trans,ion),'f3bri'
            embri(j)=embri(j)+f3bri(trans,ion)
          endif
        enddo
      enddo
c
c     Add old intercombination lines to vector EMDIF.
c
      do i=1,mlines
        j=lcbin(i)
        if ((j.ne.0).and.(fsbri(i).gt.epsilon)) then
          energ=e12fs(i)
cc          write (lulin,*) plk*cls/energ*1.d8,fsbri(i),'fsbri'
          embri(j)=embri(j)+fsbri(i)
        endif
      enddo
c
C     do i=1,xilines
C       j=xibin(i)
C       if ((j.gt.0).and.(xibri(i).gt.epsilon)) then
C         energ=ev*xiejk(i)
C         wid=evplk*(photev(j+1)-photev(j))
C         emidif(j)=emidif(j)+(xibri(i)/(wid*energ))
C       endif
C     enddo
c
C     do i=1,xhelines
C       j=xhebin(i)
C       if ((j.gt.0).and.(xhebri(i).gt.epsilon)) then
C         energ=ev*xiejk(i)
C         wid=evplk*(photev(j+1)-photev(j))
C         emidif(j)=emidif(j)+(xhebri(i)/(wid*energ))
C       endif
C     enddo
c
c old intercombination lines
c
      do i=1,nlines
        j=lrbin(i)
        if ((j.ne.0).and.(rbri(i).gt.epsilon)) then
          energ=e12r(i)
cc          write (lulin,*) plk*cls/energ*1.d8,rbri(i),'rbri'
          embri(j)=embri(j)+rbri(i)
        endif
      enddo
c Original He I lines, just the first three
      do i=1,3
        j=heibin(i)
        if ((j.ne.0).and.(heibri(i).gt.epsilon)) then
          energ=ev*(lmev/(heilam(i)*1.d8))
cc          write (lulin,*) heilam(i)*1.d8,heibri(i),'heibri'
          embri(j)=embri(j)+heibri(i)
        endif
      enddo
c
c New He I singlet lines
c
      do i=1,nheislines
        j=heisbin(i)
        if ((j.ne.0).and.(heisbri(i).gt.epsilon)) then
          energ=heiseij(i)
cc          write (lulin,*) plk*cls/energ*1.d8,heisbri(i),'heisbri'
          embri(j)=embri(j)+heisbri(i)
        endif
      enddo
c
c New He I triplet lines
c
      do i=1,nheitlines
        j=heitbin(i)
        if ((j.ne.0).and.(heitbri(i).gt.epsilon)) then
          energ=heiteij(i)
cc          write (lulin,*) plk*cls/energ*1.d8,heitbri(i),'heitbri'
          embri(j)=embri(j)+heitbri(i)
        endif
      enddo
c
c heavy element recomb lines
c
      do i=1,nrccii
        j=rccii_bin(i)
        if ((j.ne.0).and.(rccii_bbri(i).gt.epsilon)) then
          energ=rccii_eij(i)
cc          write (lulin,*) plk*cls/energ*1.d8,rccii_bbri(i),'rccii_bbri'
          embri(j)=embri(j)+rccii_bbri(i)
        endif
      enddo
c
      do i=1,nrcnii
        j=rcnii_bin(i)
        if ((j.ne.0).and.(rcnii_bbri(i).gt.epsilon)) then
          energ=rcnii_eij(i)
cc          write (lulin,*) plk*cls/energ*1.d8,rcnii_bbri(i),'rcnii_bbri'
          embri(j)=embri(j)+rcnii_bbri(i)
        endif
      enddo
c
      do i=1,nrcoi_q
        j=rcoi_qbin(i)
        if ((j.ne.0).and.(rcoi_qbbri(i).gt.epsilon)) then
          energ=rcoi_qeij(i)
cc          write (lulin,*) plk*cls/energ*1.d8,rcoi_qbbri(i),'rcoi_qbbri'
          embri(j)=embri(j)+rcoi_qbbri(i)
        endif
      enddo
c
      do i=1,nrcoi_t
        j=rcoi_tbin(i)
        if ((j.ne.0).and.(rcoi_tbbri(i).gt.epsilon)) then
          energ=rcoi_teij(i)
cc          write (lulin,*) plk*cls/energ*1.d8,rcoi_tbbri(i),'rcoi_tbbri'
          embri(j)=embri(j)+rcoi_tbbri(i)
        endif
      enddo
c
      do i=1,nrcoii
        j=rcoii_bin(i)
        if ((j.ne.0).and.(rcoii_bbri(i).gt.epsilon)) then
          energ=rcoii_eij(i)
cc          write (lulin,*) plk*cls/energ*1.d8,rcoii_bbri(i),'rcoii_bbri'
          embri(j)=embri(j)+rcoii_bbri(i)
        endif
      enddo
c
      do i=1,nrcneii
        j=rcneii_bin(i)
        if ((j.ne.0).and.(rcneii_bbri(i).gt.epsilon)) then
          energ=rcneii_eij(i)
cc          write (lulin,*) plk*cls/energ*1.d8,rcneii_bbri(i)
cc     &     ,'rcneii_bbri'
          embri(j)=embri(j)+rcneii_bbri(i)
        endif
      enddo
c
c     adds xr3lines to xr3lines_emilin(1,line)
c
      do line=1,nxr3lines
        xr3lines_emilin(1,line)=0.d0
        if (xr3lines_bri(line).gt.epsilon) then
          j=xr3lines_bin(line)
          if (j.ne.0) then
          energ=xr3lines_egij(line)
          wid=evplk*(photev(j+1)-photev(j))          
cc          write (lulin,*) plk*cls/energ*1.d8,xr3lines_bri(line)
cc     &     ,'xr3lines_bri'
          xr3lines_emilin(1,line)=xr3lines_bri(line)/(wid*energ)
          embri(j)=embri(j)+xr3lines_bri(line)
        endif
        endif
      enddo
c
c     adds xrllines to xrllines_emilin(1,line)
c
      do line=1,nxrllines
        xrllines_emilin(1,line)=0.d0
        if (xrllines_bri(line).gt.epsilon) then
          j=xrllines_bin(line)
          if (j.ne.0) then
          energ=xrllines_egij(line)
          wid=evplk*(photev(j+1)-photev(j))
cc          write (lulin,*) plk*cls/energ*1.d8,xrllines_bri(line)
cc     &     ,'xrllines_bri'          
          xrllines_emilin(1,line)=xrllines_bri(line)/(wid*energ)
          embri(j)=embri(j)+xrllines_bri(line)
        endif
        endif
      enddo
c
c     adds xlines to emilin(1,line)
c
C     do line=1,xlines
C       emilin(1,line)=0.d0
C       if (xrbri(line).gt.epsilon) then
C         j=xbin(line)
C         if (j.ne.0) then
C         energ=ev*xejk(line)
C         wid=evplk*(photev(j+1)-photev(j))
C         emilin(1,line)=xrbri(line)/(wid*energ)
C       endif
C       endif
C     enddo
c
c
c     adds hydrogen and helium lines to hydlin(1,line,series) and
c      hellin(1,line,series)
c
c     hydlin -> Hydrogen recombination line
c
      do series=1,nhseries
        do line=1,nhlines
          hydlin(1,line,series)=0.d0
          if (hydrobri(line,series).gt.epsilon) then
          j=hbin(line,series)
          if (j.ne.0) then
            energ=ev*lmev/hlambda(line,series)
            wid=evplk*(photev(j+1)-photev(j))
cc            write (lulin,*) plk*cls/energ*1.d8,hydrobri(line,series)
cc     &     ,'hydrobri'               
            hydlin(1,line,series)=hydrobri(line,series)/(wid*energ)
            embri(j)=embri(j)+hydrobri(line,series)
          endif
          endif
        enddo
      enddo
c
c     hellin -> Helium recombination line
c
      do series=1,nheseries
        do line=1,nhelines
          hellin(1,line,series)=0.d0
          if (helibri(line,series).gt.epsilon) then
          j=hebin(line,series)
          if (j.ne.0) then
            energ=ev*lmev/helambda(line,series)
            wid=evplk*(photev(j+1)-photev(j))
cc            write (lulin,*) plk*cls/energ*1.d8,helibri(line,series)
cc     &     ,'helibri'               
            hellin(1,line,series)=helibri(line,series)/(wid*energ)
            embri(j)=embri(j)+helibri(line,series)
          endif
          endif
        enddo
      enddo
c
c     Adds heavy hydrogenic series
c
      if (atypes.gt.3) then
        do atom=3,atypes
c
          nz=mapz(atom)
c
          do series=1,nxhseries
            do line=1,nxhlines
              xhydlin(1,line,series,atom)=0.d0
              if (xhydrobri(line,series,atom).gt.epsilon) then
              j=xhbin(line,series,atom)
              if (j.ne.0) then
c            energ = 0.5d0*(ev*(photev(j+1)+photev(j)))
                energ=ev*lmev/xhlambda(line,series,atom)
                wid=evplk*(photev(j+1)-photev(j))
cc            write (lulin,*) plk*cls/energ*1.d8
cc     &       ,xhydrobri(line,series,atom),'xhydrobri'                 
                xhydlin(1,line,series,atom)=xhydrobri(line,series,atom)/
     &           (wid*energ)
                embri(j)=embri(j)+xhydrobri(line,series,atom)
              endif
              endif
            enddo
          enddo
c      endif
        enddo
      endif
c
cc      close(lulin)
c
      rectot=0.0d0
c
      do inl=1,infph-1
         recpdf(inl)=0.0d0
         if ((photev(inl).ge.mcnuin).and.(photev(inl).lt.mcnuend))
     &   rectot=rectot+embri(inl)         
      enddo   
c      
      do inl=1,infph-1
         recpdf(inl)=0.0d0
         if ((photev(inl).ge.mcnuin).and.(photev(inl).lt.mcnuend)) then
            if (inl.eq.1) then 
               recpdf(inl)=embri(inl)/rectot
            else   
               recpdf(inl)=recpdf(inl-1)+embri(inl)/rectot
            endif
         endif   
      enddo
c
      return
c
      end
