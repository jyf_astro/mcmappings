cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c     MAPPINGS V.  An Astrophysical Plasma Modelling Code.
c
c     copyright 1994 Ralph S. Sutherland, Michael A. Dopita
c     Luc Binette and Brent Groves
c
c       Version v5.1.13
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c*******PHOTOIONISATIONMODEL
c     DIFFUSE FIELD CALCULATED ; 'OUTWARD ONLY' INTEGRATION, RADIATION
c     PRESSURE
c
c     space steps derived from optical depth of predicted temperature
c     and ionistation, extrapolated from previous steps
c
c     CHOICE OF EQUILIBRIUM OR FINITE AGE CONDITIONS
c
c     NB. COMPUTATIONS PERFORMED IN SUBROUTINE COMPPH6
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
      subroutine montphoto7 ()
c
      include 'cblocks.inc'
c
c           Variables
c
      real*8 blum,ilum,dhlma,dhn,dht,difma,dthre,dti,dtlma
      real*8 epotmi,fin,uinit, qinit, rcin
      real*8 rstsun,wid,starlum
      real*8 scale,dnt,lam
c      real*8 fluxes(mxmonlines)
c
      integer*4 j,luin,i,kmax,k,nl
      integer*4 idx1 ,idx2, idx3, idx4, iem
c      character=jtb*4
      character carac*36,model*32
      character caract*4,ilgg*4
      character banfil*32
c
c           Functions
c
      real*8 densnum,fdilu
c
      luin=10
      jcon='YES'
      jspot='NO'
      dtau0=0.025d0
      nprefix=6
c
      write (*,10)
c
c     ***INITIAL IONISATION CONDITIONS
c
   10 format(///
     &     ' *********************************************************'/
     &     '  Photoionisation model Monte-Carlo selected:',/,
     &     ' *********************************************************')
c
      epotmi=iphe
c
c     set up ionisation state
c
      model='protoionisation'
      call popcha (model)
c
c     ***CHOOSING PHOTON SOURCE AND GEOMETRICAL PARAMETERS
c
      model='Photo 6'
c
      call photsou (model)
c
c     Possible to have no photons
c
c      qhlo = dlog(qht+epsilon)
c      rechy = 2.6d-13
c     reclo = dlog(rechy)
c
      alphahb=2.585d-13
      alphaheb=1.533d-12
c
c Default outward only intgration assuming spherical symmetric nebula
cc
   20 write (*,40)
   30 format(a)
   40 format(/' Input Density File : ',$)
      read (*,30) fnam

      m=lenv(fnam)
      denfile=fnam(1:m)
c
      rstar=1.d0
      astar=rstar*rstar
c

      blum=0.d0
      ilum=0.d0
c
      do i=1,infph-1
         wid=photev(i+1)-photev(i)
         blum=blum+soupho(i)*wid*evplk
         if (photev(i).ge.iph) ilum=ilum+soupho(i)*wid*evplk
      enddo
c
      blum=pi*blum
      ilum=pi*ilum
c
      rstar=1.d0
      astar=rstar*rstar
c
      if (blum.gt.0.d0) then
c
   50    write (*,60)
   60    format(/' Define the source size or luminosity '/
     &           ' ::::::::::::::::::::::::::::::::::::::::::::::::::::'/
     &           '    R   :  By Radius'/
     &           '    L   :  By Luminosity'/
     &           '    P   :  By Ionizing Photons'//
     &           ' :: ',$)
         read (*,30) ilgg
         ilgg=ilgg(1:1)
c
         if (ilgg.eq.'r') ilgg='R'
         if (ilgg.eq.'l') ilgg='L'
         if (ilgg.eq.'p') ilgg='P'
c
         if ((ilgg.ne.'R').and.(ilgg.ne.'L').and.(ilgg.ne.'P')) goto
     &   50
c
         if (ilgg.eq.'R') then
c
   70       format(/,/,' Give photoionisation source radius'
     &                ,'(Rsun=6.96e10)',/
     &                ,' (in solar units (<1.e5) or in cm (>1.e5) : ',$)
            write (*,70)
            read (*,*) rstsun
            if (rstsun.le.0.0d0) goto 50
            if (rstsun.lt.1.d5) then
              rstar=6.96d10*rstsun
            else
              rstar=rstsun
              rstsun=rstar/6.96d10
            endif
c
            astar=fpi*rstar*rstar
            blum=astar*blum
            ilum=astar*ilum
c
   80       format(//' ****************************************************'/
     &               '  Source Total Luminosity              : ',1pg12.5/
     &               '  Source Ionising (13.6eV+) Luminosity : ',1pg12.5/
     &               '  Source Ionising (13.6eV+) Photons    : ',1pg12.5/
     &               ' ****************************************************')
            write (*,80) blum,ilum,qht*astar
c
c     end def by radius
c
         endif
c
         if (ilgg.eq.'L') then
c
   90       format(//' Total or Ionising Luminosity (T/I)',$)
            write (*,90)
            read (*,30) ilgg
            ilgg=ilgg(1:1)
c
            if (ilgg.eq.'t') ilgg='T'
            if (ilgg.eq.'i') ilgg='I'
c
            if ((ilgg.ne.'I').and.(ilgg.ne.'T')) ilgg='I'
c
  100       format(//' Give ionising source luminosity '/
     &               ' (log (<100) or ergs/s (>100)) : ',$)
  110       format(//' Give bolometric source luminosity '/
     &               ' (log (<100) or ergs/s (>100)) : ',$)
            if (ilgg.eq.'T') then
              write (*,110)
            else
              write (*,100)
            endif
            read (*,*) starlum
            if (starlum.lt.1.d2) starlum=10.d0**starlum
c
c
            if (ilgg.eq.'I') then
              rstsun=dsqrt(starlum/(ilum*fpi))
            else
              rstsun=dsqrt(starlum/(blum*fpi))
            endif
c
c
  120       format(//' ****************************************************'/
     &               '  Source Radius : ',1pg12.5,' cm'/
     &               ' ****************************************************')
            write (*,120) rstsun
            astar=fpi*rstsun*rstsun
            blum=astar*blum
            ilum=astar*ilum
            write (*,80) blum,ilum,qht*astar
c
c
            rstar=rstsun
            rstsun=rstar/rsun
            astar=fpi*rstar*rstar
c
c     end def by luminosity
c
         endif
c
         if (ilgg.eq.'P') then
c
  130       format(//' Give source ionising photon rate '/
     &               ' (log (<100) or photons/s (>100)) : ',$)
            write (*,130)
            read (*,*) starlum
            if (starlum.lt.1.d2) starlum=10.d0**starlum
c
c     P/s = qht*fpi*rstsun*rstsun
c
            rstsun=dsqrt(starlum/(qht*fpi))
c
            write (*,120) rstsun
            astar=fpi*rstsun*rstsun
            blum=astar*blum
            ilum=astar*ilum
            write (*,80) blum,ilum,qht*astar
c
            rstar=rstsun
            rstsun=rstar/rsun
            astar=fpi*rstar*rstar
c
c    end source radius with photons
         endif

  131    format(//' Give initial energy packet number, '/
     &            ' increasing factor, convergence plateau : ',$)
         write (*,131)
         read (*,*) npck, npckinc, convrt

         mcdengy = ilum/npck

  132    format(//' ****************************************************'/
     &            '  Number of Energy Packet              : ',1pg12.5/
     &            '  Delta Energy                         : ',1pg12.5/
     &            ' ****************************************************')
         write (*,132) npck, mcdengy
c    end blum>0
      endif
c
c
  140 format(//' ::::::::::::::::::::::::::::::::::::::::::::::::::::'/
     &     '  Setting the Physical Structure  '/
     &     ' ::::::::::::::::::::::::::::::::::::::::::::::::::::'//)
      write (*,140)
c
c     ***THERMAL STRUCTURE
c
      jthm='S'

c     ***DENSITY BEHAVIOR
c
c default partial pressure of Hydrogen, ignored if not isobaric
c
      ponk=1.d6
c
  150 write (*,160)
  160 format(/'  Choose a Density Structure '/
     &     ' ::::::::::::::::::::::::::::::::::::::::::::::::::::'/
     &     '    C  : isoChoric,  (const volume) (Only Available)'/
     &     '    B  : isoBaric, (const pressure)'/
     &     ' :: ',$)
      read (*,30) jden
c
      if ((jden(1:1).eq.'C').or.(jden(1:1).eq.'c')) jden='C'
      if ((jden(1:1).eq.'B').or.(jden(1:1).eq.'b')) jden='B'
c
      if ((jden.ne.'C').and.(jden.ne.'B')) goto 150
c
c     Estimate strom radius for nominal density
c     if we have any photons that is
c
c     if isobaric, get pressure regime

      dht=densnum(dhn)
c
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      if (qht.gt.0.d0) then

         volstromhb=(qht*astar)/(dhn*dht*alphahb*fin)
         rstromhb=(3.d0*volstromhb/fpi)**0.333333333333d0
         volstromheb=(qheii*astar)/(zion(2)*dhn*dht*alphaheb*fin)
         rstromheb=(3.d0*volstromheb/fpi)**0.333333333333d0
c
         rmax=dmax1(rstromhb,rstromheb)
c
         qhdnin=qht/dht
         qhdnav=(4.d0*qht/dht)*fdilu(rstar,(rmax+rstar)*0.5d0)
c
         unin=qhdnin/cls
         unav=qhdnav/cls
c
         qhdhin=qht/dhn
         qhdhav=qhdnav*(dht/dhn)
c
         uhin=qhdhin/cls
         uhav=qhdhav/cls
c
  170    write (*,180) rstromhb,rstromheb,qht*astar,qheii*astar,qht,
     &     qheii,qhdnin,qhdhin,qhdnav,qhdhav,unin,uhin,unav,uhav
  180    format(//,
     &          ' ****************************************************',/,
     &          '   Filled Sphere Parameters:',/,
     &          ' ****************************************************',/,
     &          '  Estimated HII   Stromgren radius:',1pg10.3,' cm.',/,
     &          '  Estimated HeIII Stromgren radius:',1pg10.3,' cm.',/,
     &          ' ****************************************************',/,
     &          '  Photon Luminosity LQH    :',1pg10.3, ' Phot/s',/,
     &          '  Photon Luminosity LQHeII :',1pg10.3, ' Phot/s',/,
     &          '  Photon Flux       FQH    :',1pg10.3, ' Phot/cm2/s',/,
     &          '  Photon Flux       FQHeII :',1pg10.3, ' Phot/cm2/s',/,
     &          ' ****************************************************',/,
     &          '        QHDN inner   : ',1pg12.5,' cm/s'/,
     &          '        QHDH inner   : ',1pg12.5,' cm/s'/,
     &          '          <QHDN>     : ',1pg12.5,' cm/s'/,
     &          '          <QHDH>     : ',1pg12.5,' cm/s'/,
     &          '        U(N) inner   : ',1pg12.5,/,
     &          '        U(H) inner   : ',1pg12.5,/,
     &          '          <U(N)>     : ',1pg12.5,/,
     &          '          <U(H)>     : ',1pg12.5,/,
     &          ' ****************************************************',//
     &          ' Give the initial radius ',
     &          ' (in cm (>1) or in fraction of Stromgren radius) : ',$)
         read (*,*) remp
c
         if (remp.lt.0.0d0) goto 170
         if (remp.le.1) remp=remp*rmax
         if (remp.lt.rstar) remp=rstar

         rstromhb=((rstromhb**3.d0)+(remp**3.d0))**0.333333333333d0
         rmax=rstromhb
c
         rstromheb=((rstromheb**3.d0)+(remp**3.d0))**0.333333333333d0
c
         blum=0.d0
         ilum=0.d0
c
         do i=1,infph-1
            wid=photev(i+1)-photev(i)
            blum=blum+soupho(i)*wid*evplk
            if (photev(i).ge.iph) ilum=ilum+soupho(i)*wid*evplk
         enddo
         blum=pi*blum
         ilum=pi*ilum
         wdpl=fdilu(rstar,remp)
         qhdnin=4.d0*qht/dht*wdpl
         unin=qhdnin/cls
         qhdhin=qhdnin*(dht/dhn)
         uhin=qhdhin/cls
         blum=blum*wdpl
         ilum=ilum*wdpl
         wdpl=fdilu(rstar,(rmax+remp)*0.5d0)
         qhdnav=4.d0*qht/dht*wdpl
         unav=qhdnav/cls
         qhdhav=qhdnav*(dht/dhn)
         uhav=qhdhav/cls

  190    format(//,
     &          ' ****************************************************',/,
     &          '   Partially Filled Sphere Parameters:',/,
     &          ' ****************************************************',/,
     &          '  Empty inner radius :',1pg12.5,' cm'/,
     &          '  Outer HII   radius :',1pg12.5,' cm.',/,
     &          '  Outer HeIII radius :',1pg12.5,' cm.',/,
     &          ' ****************************************************',/,
     &          '        QHDN inner   : ',1pg12.5,' cm/s'/,
     &          '        QHDH inner   : ',1pg12.5,' cm/s'/,
     &          '          <QHDN>     : ',1pg12.5,' cm/s'/,
     &          '          <QHDH>     : ',1pg12.5,' cm/s'/,
     &          '        U(N) inner   : ',1pg12.5,/,
     &          '        U(H) inner   : ',1pg12.5,/,
     &          '          <U(N)>     : ',1pg12.5,/,
     &          '          <U(H)>     : ',1pg12.5,/,
     &          '   Total intensity   : ',1pg12.5,' erg/s/cm2' /,
     &          '   Ionizing intensity: ',1pg12.5,' erg/s/cm2'/,
     &          ' ****************************************************',/)
          write (*,190) remp,rstromhb,rstromheb,qhdnin,qhdhin,qhdnav,
     &     qhdhav,unin,uhin,unav,uhav,blum,ilum

          radin=0.d0
          radout=1.d38
c
c     end with photons (qht>0)
        endif
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
  200 admach=0.0d0
      turbheatmode=0

      frlum=0.0d0
      tm00=0.0d0
c
c     ***EQUILIBRIUM***
      jeq='E'
c
      dtau0 = 1d0

      difma=0.05d0
      dtlma=0.10d0
      dhlma=0.050d0
c
c     ***PRINT SET UP

      write (*,210) dtau0
  210 format(//' Model Summary :',/
     &        ,' Mode  : Thermal and Ionic Equilibrium ',/
     &        ,' dTau  :',0pf8.5)

      
c
  220 format(/' Radiation Field and Parameters:',//,
     & t5, ' At estimated T_inner :',1pg10.3,' K'/
     & t5,' Rsou.',t20,' Remp.',t35,' Rmax',t50,' <DILU>'/
     & t5, 1pg10.3,t20,1pg10.3,t35,1pg10.3,t50,1pg10.3/
     & t5,' <Hdens>',t20,' <Ndens>',t35,' Fill Factor',/
     & t5, 1pg10.3,t20,1pg10.3,t35,0pf9.6,//
     & t5,' FQ tot',t20,' Log FQ',t35,' LQ tot',t50,' Log LQ',/
     & t5, 1pg10.3,t20,0pf8.3,t35,1pg10.3,t50,0pf8.3,/
     & t5,' Q(N) in',t20,' Log Q(N)',t35,' <Q(N)>',t50,' Log<Q(N)>',/
     & t5, 1pg10.3,t20,0pf8.3,t35,1pg10.3,t50,0pf8.3,/
     & t5,' Q(H) in',t20,' Log Q(H)',t35,' <Q(H)>',t50,' Log<Q(H)>',/
     & t5, 1pg10.3,t20,0pf8.3,t35,1pg10.3,t50,0pf8.3,/
     & t5,' U(N) in',t20,' Log U(N)',t35,' <U(N)>',t50,' Log<U(N)>',/
     & t5, 1pg10.3,t20,0pf8.3,t35,1pg10.3,t50,0pf8.3,/
     & t5,' U(H) in',t20,' Log U(H)',t35,' <U(H)>',t50,' Log<U(H)>',/
     & t5, 1pg10.3,t20,0pf8.3,t35,1pg10.3,t50,0pf8.3)
c
      write (*,220) tinner,rstar,remp,rmax,wdpl,dhn,dht,fin,qht,
     &dlog10(qht),astar*qht,dlog10(astar*qht),qhdnin,dlog10(qhdnin),
     &qhdnav,dlog10(qhdnav),qhdhin,dlog10(qhdhin),qhdhav,dlog10(qhdhav),
     &unin,dlog10(unin),unav,dlog10(unav),uhin,dlog10(uhin),uhav,
     &dlog10(uhav)
c
c     ***TYPE OF EXIT FROM THE PROGRAM
c
  230 ielen=1
      jpoen=1
      tend=10.0d0
      fren=0.01d0
      diend=0.0d0
      tauen=0.0d0
c
c
  240 format(//' ::::::::::::::::::::::::::::::::::::::::::::::::::::'/
     &          '  Boundry Conditions  '/
     &          ' ::::::::::::::::::::::::::::::::::::::::::::::::::::')
      write (*,240)
c
  250 format(/'  Choose a model ending : '/
     &     ' ::::::::::::::::::::::::::::::::::::::::::::::::::::'/
     &     '    A  :   Radiation bounded, HII <',2pf5.2,'%'/
     &     '    E  :   Density bounded, Distance'/
     &     ' :: ',$)
      write (*,250) fren
      read (*,30) jend
c
      if ((jend(1:1).eq.'A').or.(jend(1:1).eq.'a')) jend='A'
      if ((jend(1:1).eq.'E').or.(jend(1:1).eq.'e')) jend='E'
c
      if ((jend.ne.'A').and.(jend.ne.'E')) goto 230

c
      if (jend.eq.'E') then
  260   write (*,270)
  270    format(/' Give the distance or radius at which the density',
     &        ' drops: '/
     &        ' (in cm (>1E6) or as a fraction of the Stromgren'/
     &        ' radius (<1E6)) : ',$)
        read (*,*) diend
        if (diend.lt.1.d6) diend=diend*rmax
        diend=remp+diend
        if (diend.le.remp) goto 260
      endif
c
  280 format(//
     &     ' ::::::::::::::::::::::::::::::::::::::::::::::::::::'/
     &     '  Output Requirements  '/
     &     ' ::::::::::::::::::::::::::::::::::::::::::::::::::::'//)
      write (*,280)
c
c
  290 format(//'  Choose output settings : '/
     &          ' :::::::::::::::::::::::::::::::::::::::::::::::::::::'/
     &          '    A  :   Standard output (photnxxxx,phapnxxxx).'/
     &          '    B  :   Standard + monitor 4 element ionisation'/
     &          '    C  :   Standard + all ions file'/
     &          '    D  :   Standard + final source + nebula spectra'/
     &          '    F  :   Standard + first balance'/
     &          '    G  :   Everything'//
     &          '    I  :   Standard + B + monitor 4 multi-level ion emission'/
     &          '    J  :   Standard + B + monitor up to 16 lines'/
     &          ' :: ',$)
      write (*,290)
      read (*,30) ilgg
c
      if ((ilgg(1:1).eq.'A').or.(ilgg(1:1).eq.'a')) ilgg='A'
      if ((ilgg(1:1).eq.'B').or.(ilgg(1:1).eq.'b')) ilgg='B'
      if ((ilgg(1:1).eq.'C').or.(ilgg(1:1).eq.'c')) ilgg='C'
      if ((ilgg(1:1).eq.'D').or.(ilgg(1:1).eq.'d')) ilgg='D'
      if ((ilgg(1:1).eq.'F').or.(ilgg(1:1).eq.'f')) ilgg='F'
      if ((ilgg(1:1).eq.'G').or.(ilgg(1:1).eq.'g')) ilgg='G'
      if ((ilgg(1:1).eq.'I').or.(ilgg(1:1).eq.'i')) ilgg='I'
      if ((ilgg(1:1).eq.'J').or.(ilgg(1:1).eq.'j')) ilgg='J'
c
      jiel='NO'
      jiem='NO'
      jlin='NO'
      if (ilgg.eq.'B') jiel='YES'
      if (ilgg.eq.'G') jiel='YES'
      if (ilgg.eq.'G') jiem='YES'
      if (ilgg.eq.'G') jlin='YES'
      if (ilgg.eq.'I') jiel='YES'
      if (ilgg.eq.'I') jiem='YES'
      if (ilgg.eq.'J') jiel='YES'
      if (ilgg.eq.'J') jlin='YES'
c
c
c     default monitor elements
c
      iel1=zmap(1)
      iel2=zmap(2)
      iel3=zmap(6)
      iel4=zmap(8)
c
      if (jiel.eq.'YES') then
  300    format(//' Give 4 elements to monitor (atomic numbers): ',$)
        write (*,300)
c
        read (*,*) iel1,iel2,iel3,iel4
        iel1=zmap(iel1)
        iel2=zmap(iel2)
        iel3=zmap(iel3)
        iel4=zmap(iel4)
c
      endif
c
      if (jiem.eq.'YES') then
c
  310 format(//' Choose 4 multi-level species #',i2,' :',/
     &          '::::::::::::::::::::::::::::::::::::::::::::::::::::'/)
        write (*,310) ,iem
        do i=1,(nfmions/5)+1
          j=(i-1)*5
          kmax=min(j+5,nfmions)-j
          write (*,'(5(2x,i3,": ",a2,a6))') (j+k,elem(fmatom(j+k)),
     &     rom(fmion(j+k)),k=1,kmax)
        enddo
  320 format(' :: ',$)
        write (*,320)
        read (*,*) idx1,idx2,idx3,idx4
        if (idx1.lt.1) idx1=1
        if (idx1.gt.nfmions) idx1=nfmions
        if (idx2.lt.1) idx2=1
        if (idx2.gt.nfmions) idx2=nfmions
        if (idx3.lt.1) idx3=1
        if (idx3.gt.nfmions) idx3=nfmions
        if (idx4.lt.1) idx4=1
        if (idx4.gt.nfmions) idx4=nfmions
        iemidx(1)=idx1
        iemidx(2)=idx2
        iemidx(3)=idx3
        iemidx(4)=idx4
      endif
c
      if (jlin.eq.'YES') then
c
  330  format(//' Choose up to ',i2,' lines by wavelength (A)   :',/
     &          '::::::::::::::::::::::::::::::::::::::::::::::::::::'/)
  340  format(' Number of lines : ',$)
  350  format(/' #',i2,' : ',$)
        write (*,330) mxmonlines
        write (*,340)
        read (*,*) nl
        nl=min(max(nl,1),mxmonlines)
        njlines=nl
        do i=1,njlines
            write (*,350) i
            read (*,*) lam
            lam=dabs(lam)
            emlinlist(i)=lam
        enddo
        do i=1,mxmonlines
           emlindeltas(i)=0.001d0 ! will allow custom bins later
        enddo
        call speclocallineids(emlinlistatom,emlinlistion)
      endif
c
      jall='NO'
      if ((ilgg.eq.'C').or.(ilgg.eq.'G')) jall='YES'
c
      jspec='NO'
      if ((ilgg.eq.'D').or.(ilgg.eq.'G')) jspec='YES'
c
      jsou='NO'
      jpfx='psend'
c
      if ((ilgg.eq.'D').or.(ilgg.eq.'G')) then
c
        jsou='YES'
c
c     get final field file prefix
c
  360   format (a16)
  370   format(//' Give a prefix for final source file : ',$)
        write (*,370)
        read (*,360) jpfx
        nprefix=1
  380 if ((jpfx(nprefix+1:nprefix+1).eq.' ')
     &     .or.(nprefix.ge.8)) goto 390
          nprefix=nprefix+1
        goto 380
  390   continue
        jpfx=jpfx(1:nprefix)
      endif
c
      jbal='NO'
      jbfx='p7bal'
c
      if ((ilgg.eq.'F').or.(ilgg.eq.'G')) then
c
        jbal='YES'
c
c     get final field file prefix
c
  400   format(//' Give a prefix for first ion balance file : ',$)
        write (*,400)
        read (*,360) jbfx
        nprefix=1
  410   if ((jbfx(nprefix+1:nprefix+1).eq.' ')
     &     .or.(nprefix.ge.8)) goto 420
          nprefix=nprefix+1
          goto 410
  420   continue
        jbfx=jbfx(1:nprefix)
      endif
c
c     get runname
c
  430 format (a96)
  440 format(//' Give a name/code for this run: ',$)
      write (*,440)
      read (*,430) runname
c
c     remains of old vax batch system (not used)
c
      banfil='INTERACTIVE'
c
      call montph7 (dhn, banfil, difma, dtlma, dhlma)
c
      return
c
      end
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c   MAPPINGS V.  An Astrophysical Plasma Modelling Code.
c
c   copyright 1979-2012
c
c       Version v5.1.13
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c       PHOTOIONISATION MODEL
c       DIFFUSE FIELD CALCULATED ; 'OUTWARD ONLY' INTEGRATION
c
c       PHOTO6: As for P5 but adds new integration scheme
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
      subroutine montph7 (dhn, banfil, difma, dtlma, dhlma)
c
      include 'cblocks.inc'
c     
c     variable
c
      integer*4 xx, yy, zz
      integer*4 ie,j,i,inl
      real*8 opac(mxinfph),abio
      real*8 t,de,dh
c
      do xx = 1, nx
      	do yy = 1, ny
      	   do zz = 1, nz        

              t = t_arr(xx,yy,zz)
              de = de_arr(xx,yy,zz)
              dh = hd_arr(xx,yy,zz)   
c
              write(*,*) t, de, dh, 'b'

c
c             Calculate ion fraction    
              call equion (t, de, dh)
c
              write(*,*) t, de, dh, 'a'
c
c             Calculate Opacity 
ccc              opac = 0.

ccc              do inl=1, infph
ccc                 do i=1,ionum                
ccc                    if (photxsec(i,inl).gt.0.d0) then
ccc                       ie=atpho(i)
ccc                       j=ionpho(i)
ccc                       abio=zion(ie)*pop(j,ie)
ccc                       if (abio.gt.1.d-12) then
ccc                          opac(inl) = opac(inl) + photxsec(i,inl)*abio
ccc                       endif
ccc                    endif
ccc                 enddo
ccc                 opac_arr(xx,yy,zz,inl) = opc(inl)
ccc              enddo
c
c             Calculate recPDF (continuum distribution) 
c             & totLine (non-ionizing photon energy)              
ccc              call emPDF (t,de,dh)       
c        
ccc              do inl=1,infph
ccc                 recPDF_arr(xx,yy,zz,inl) = recPDF(inl)
ccc              enddo
c
ccc              totLine_arr(xx,yy,zz) = totLine
c              
           enddo
      	enddo
      enddo
c      
c     Radiative Transfer

ccc      call ePacRT ()

c
c     Te, eDen & ionDen Equillibrium
cc      call updateCell
c
      return
c
      end
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c     Subroutine to transfer energy packet
c
c            OUTPUT: Jnu(?)
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
      subroutine ePackRT ()
c
      include 'cblocks.inc'
      include 'grids.inc'
c    
      logical   lgstr, lgdif, lgescape, lgabs
      integer*4 absEvent, seed0, seed
      integer*4 iphot
c      
      real*8    rvec(3), rvectmp(3), Hvec(3)
      real*8    drvec, drvectmp
c
      real*8    abstau, tauloc
      real*8    passprob, random
c
      integer*4 cwi(6),ci(3)
      real*8    cw
c
      real*8    dS,dSx,dSy,dSz
      real*8    dv
c
      real*8    a,b,c
c
c
      jnu_arr = 0.0d0
c
      do iphot = 1, npck
c
         lgstr = .true.
         lgdif = .false.
         lgescape = .false.
c      
         xorg=strpos(1)      ! initial position of photon
         yorg=strpos(2)
         zorg=strpos(3)
c
         absEvent = 0
c
   20    if ((.not.lgescape).and.(absEvent.le.safelmt)) then 
c
   30       if (.not.(lgstr.or.lgdif)) then
               write(*,*)'Neither Stellar nor Diffuse Emission'                        
               stop
            else 
               if (lgstr.and.(.not.lgdif)) then  ! stellar photon
                  call nwphtn (lgstr,lgdif,nuP,Hvec)       ! get Nu & Direction           
               endif
               if ((.not.lgstr).and.lgdif) then  ! diffuse photon
                  call getRand
                  if (randm.ge.totLine) then 
                     lgescape = .true.
                     goto 20
                  endif
                  call nwphtn (lgstr,lgdif,nuP,Hvec)       ! get Nu & Direction  
               endif     
               lgabs=.false.          
            endif
c            
c           Initialize position
            rvec(1)=xorg
            rvec(2)=yorg
            rvec(3)=zorg
c
            drvec=0.0d0
            drvec=dsqrt(rvec(1)**2.0d0+rvec(2)**2.0d0+rvec(3)**2.0d0)
c 
            call gtcwi(rvec,cwi)    ! get cell wall index
c
            if ((nuP.lt.ionEdge).or.(drvec.gt.diend)) then 
               lgescape = .true.
               goto 20
            endif
c
            abstau=0.0d0
            seed0=86432
            seed=seed0
            random=ran2(seed)            
            passprob=-log(1.-random)
c
c           Start path loop
c           Looking for dSx
  300       if (dabs(Hvec(1)).le.1d-10) then
               dSx = cw(xst,dx,nx+1)
               ci(1) = cwi(1)
            else 
               if (Hvec(1).gt.1d-10) then
                  ci(1) = cwi(1)
                  dSx = (cw(xst,dx,ci+1)-rvec(1))/Hvec(1)
               else
                  if (Hvec(1).lt.-1d-10) then
                     ci(1) = cwi(2)-1
                     dSx = (cw(xst,dx,ci)-rvec(1))/Hvec(1)
                  else
                     write(*,*)'Wrong Dirction Vector in X'                    
                     stop
                  endif
               endif   
            endif
c           Looking for dSx
            if (dabs(Hvec(2)).le.1d-10) then
               dSy = cw(yst,dy,ny+1)
               ci(2) = cwi(3)
            else 
               if (Hvec(2).gt.1d-10) then
                  ci(2) = cwi(3)
                  dSy = (cw(yst,dy,ci+1)-rvec(2))/Hvec(2)
               else
                  if (Hvec(2).lt.-1d-10) then
                     ci(2) = cwi(4)-1
                     dSy = (cw(yst,dy,ci)-rvec(2))/Hvec(2)
                  else
                     write(*,*)'Wrong Dirction Vector in Y'                    
                     stop
                  endif
               endif   
            endif
c           Looking for dSx
            if (dabs(Hvec(3)).le.1d-10) then
               dSz = cw(zst,dz,nz+1)
               ci(3) = cwi(5)
            else 
               if (Hvec(3).gt.1d-10) then
                  ci(3) = cwi(5)
                  dSz = (cw(zst,dz,ci+1)-rvec(3))/Hvec(3)
               else
                  if (Hvec(3).lt.-1d-10) then
                     ci(3) = cwi(6)-1
                     dSz = (cw(zst,dz,ci)-rvec(3))/Hvec(3)
                  else
                     write(*,*)'Wrong Dirction Vector in Z'                    
                     stop
                  endif
               endif   
            endif
c           
            dS = 0.0d0
            dS = min(dSx,dSy,dSz) 
            dv = 0.0d0
            dv = ((cw(xst,dx,ci(1)+1)-cw(xst,dx,ci(1)))
      &         *((cw(yst,dy,ci(2)+1)-cw(yst,dy,ci(2)))
      &         *((cw(zst,dz,ci(3)+1)-cw(zst,dy,ci(3)))   
c
            rvectmp(1) = 0.0d0
            rvectmp(2) = 0.0d0
            rvectmp(3) = 0.0d0
            rvectmp(1) = rvec(1)+dS*Hvec(1)
            rvectmp(2) = rvec(2)+dS*Hvec(2)
            rvectmp(3) = rvec(3)+dS*Hvec(3)
c
            drvectmp=0.0d0
            drvectmp=dsqrt(rvectmp(1)**2.0d0
      &                   +rvectmp(2)**2.0d0
      &                   +rvectmp(3)**2.0d0) 
c
c           Whether photon reach Rin (remp)
            if (drvectmp.lt.remp) then
               cwi = 0.0d0
               call gtcwi(rvectmp,cwi) 
               goto 300        
            endif            
c
c           Whether photon out of Rout (diend)
            if (drvectmp.ge.diend) then
               a=1.0d0
               b=2.0d0*(rvec(1)*Hvec(1)+rvec(2)*Hvec(2)+rvec(3)*Hvec(3))
               c=(drvec**2.0d0-diend**2.0d0)
               dS=(dsqrt(b**2.0d0-4.0d0*a*c)-b)/(2.0d0*a)
            endif
c
c           Calculate local Tau
            tauloc=0.0d0
c            
            if (opac_arr(ci(1),ci(2),ci(3),nuP).gt.0)
      &        tauloc=dS*opac_arr(ci(1),ci(2),ci(3),nuP)
c      
            if (((tauloc+abstau).gt.passprob).and.(tauloc.gt.0.0d0)) then
c
               dS = (passprob-abstau)/opac_arr(ci(1),ci(2),ci(3),nuP)
               rvec(1)=rvec(1)+dS*Hvec(1)
               rvec(2)=rvec(2)+dS*Hvec(2)
               rvec(3)=rvec(3)+dS*Hvec(3)
c               
               jnu_arr(ci(1),ci(2),ci(3),nuP)
      &             =jnu_arr(ci(1),ci(2),ci(3),nuP)+dS*mcdengy/dv
c
               lgstr=.false.
               lgdif=.true.
c        
               drvec=0.0d0
               drvec=dsqrt(rvec(1)**2.0d0+rvec(2)**2.0d0+rvec(3)**2.0d0)
               if (drvec.ge.diend) then
                  lgescape=.true.
               else
                  lgabs=.true.
               endif   
c                          
            else
c
               rvec(1)=rvec(1)+dS*Hvec(1)
               rvec(2)=rvec(2)+dS*Hvec(2)
               rvec(3)=rvec(3)+dS*Hvec(3)
c               
               jnu_arr(ci(1),ci(2),ci(3),nuP)
      &             =jnu_arr(ci(1),ci(2),ci(3),nuP)+dS*mcdengy/dv
c
               drvec=0.0d0
               drvec=dsqrt(rvec(1)**2.0d0+rvec(2)**2.0d0+rvec(3)**2.0d0)
               if (drvec.ge.diend) then
                  lgescape=.true.
               else
                  cwi = 0.0d0
                  call gtcwi(rvec,cwi) 
                  goto 300  
               endif   
            endif
c
            if (lgabs) then
               if(lgescape) then 
                 write(*,*)'Wrong in photon travelling'                    
                 stop  
               endif              
               absEvent=absEvent+1
               xorg=rvec(1)
               yorg=rvec(2)
               zorg=rvec(3)
               goto 30
            else
               if(lgescape) then 
                 goto 20
               else
                 write(*,*)'Wrong in photon travelling'                    
                 stop  
               endif
            endif            
         endif ! escape
c
      enddo  ! iphot
c
      return
c
      end     
c
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c     Get NEW PHOTON 
c    
c     INPUT:  PDF 
c          
c     OUTPUT: nuP  -- pointer of nu
c             Hvec -- vector of photon direction
c         
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
      subroutine nwphtn(lgstr,lgdif,nuP,Hvec)
c
      logical     lgstr, lgdif
      integer*4   i, seed0
      real*8      random
      real*8      Hvec(3)      
c
c     Get nuP 
c
c     Get random number
c     seed0=86432
      i = 0
  400 seed0 = time() 
      random = ran2(seed0)
      i = i + 1
      if ((random.ge.0.9999).or.(random.le.0).or.(i.gt.1000)) 
      &  goto 400
c
      nuP = 0
  500 nuP = nuP + 1
c     stellar photon      
      if (lgstr.and.(.not.lgdif).and.(random.ge.strPDF(nuP))) goto 500
c     diffuse photon      
      if ((.not.lgstr).and.lgdif.and.(random.ge.recPDF(nuP))) goto 500  
c
      write(*,*) nuP, random   
c
c
c     Get Hvec
c
c     Get random number
c     seed0=86432
      seed0 = time() 
      random = ran2(seed0)
      w = 2.*random - 1.
      t = sqrt(1.-w*w)
      seed0 = time() 
      random = ran2(seed0)
      ang = 3.141592654*(2.*random-1.)
      u = t*cos(ang)
      v = t*sin(ang)
      Hvec(1) = u
      Hvec(2) = v
      Hvec(3) = w            
c
c
      return
c
      END
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c     Get Cell Wall Index CWI(6) 
c    
c     INPUT:  rvec -- photon position vector 
c          
c     OUTPUT: cwi  -- cell wall index
c             CWI(1),CWI(2): Left & Right wall in X
c             CWI(3),CWI(4): Left & Right wall in Y
c             CWI(5),CWI(6): Left & Right wall in Z
c         
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      subroutine gtcwi(rvec,cwi)
c
      include 'grids.inc'
c
      real*8    rvec(3)
      real*8    cw
      integer*4 cwi(6)  
      integer*4 i
c
c
      cwi=0
      cw=0.0d0
c
      i=1   
  600 if (rvec(1).lt.cw(xst,dx,i)) then 
         write(*,*)'Wrong in photon position-X'                    
         stop
      endif  
      if (dabs(rvec(1)-cw(xst,dx,i)).le.1d-10) then 
         cwi(1)=i                          
         cwi(2)=i  
      else
         if (rvec(1).lt.cw(xst,dx,i+1)) then
            cwi(1)=i                 
            cwi(2)=i+1         
         else
            if (i.lt.nx) then
               i=i+1
               goto 600
            else 
               if ((dabs(rvec(1)-cw(xst,dx,i+1)).le.1d-10)
      &            .and.((i+1).eq.(nx+1))) then
                  cwi(1)=i+1
                  cwi(2)=i+1
               else
                  write(*,*)'Cannot find cell wall in X'                    
                  stop             
               endif 
            endif  
         endif              
      endif         
c
c
      i=1   
  700 if (rvec(2).lt.cw(yst,dy,i)) then 
         write(*,*)'Wrong in photon position-Y'                    
         stop
      endif  
      if (dabs(rvec(2)-cw(yst,dy,i)).le.1d-10) then 
         cwi(3)=i                          
         cwi(4)=i  
      else
         if (rvec(2).lt.cw(yst,dy,i+1)) then
            cwi(3)=i                 
            cwi(4)=i+1         
         else
            if (i.lt.nx) then
               i=i+1
               goto 700
            else 
               if ((dabs(rvec(2)-cw(yst,dy,i+1)).le.1d-10)
      &            .and.((i+1).eq.(nx+1))) then
                  cwi(3)=i+1
                  cwi(4)=i+1
               else
                  write(*,*)'Cannot find cell wall in Y'                    
                  stop             
               endif 
            endif  
         endif              
      endif   
c
c
      i=1   
  800 if (rvec(3).lt.cw(zst,dz,i)) then 
         write(*,*)'Wrong in photon position-Z'                    
         stop
      endif  
      if (dabs(rvec(3)-cw(zst,dz,i)).le.1d-10) then 
         cwi(5)=i                          
         cwi(6)=i  
      else
         if (rvec(3).lt.cw(zst,dz,i+1)) then
            cwi(5)=i                 
            cwi(6)=i+1         
         else
            if (i.lt.nx) then
               i=i+1
               goto 800
            else 
               if ((dabs(rvec(3)-cw(zst,dz,i+1)).le.1d-10)
      &            .and.((i+1).eq.(nx+1))) then
                  cwi(5)=i+1
                  cwi(6)=i+1
               else
                  write(*,*)'Cannot find cell wall in Z'                    
                  stop             
               endif 
            endif  
         endif              
      endif  
c  
      return
c
      end
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      real*8 function cw(stp,interv,ind)
c
      implicit none
c
      integer*4 ind
      real*8 stp,interv  
c
      cw = stp+(ind-1)*interv
c
      return
c     
      end
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c     Random Number Generator ran2 in Numerical Recipe
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      function ran2(idum)
      integer*4 idum,im1,im2,imm1,ia1,ia2,iq1,iq2,ir1,ir2,ntab,ndiv
      real*8 ran2,am,eps,rnmx
      parameter (im1=2147483563,im2=2147483399,am=1.0d0/im1,imm1=im1-1,
     &           ia1=40014,ia2=40692,iq1=53668,iq2=52774,ir1=12211,
     &           ir2=3791,ntab=32,ndiv=1+imm1/ntab,eps=1.2d-7,
     &           rnmx=1.0d0-eps)
      integer idum2,j,k,iv(ntab),iy
      save iv,iy,idum2
      data idum2/123456789/, iv/ntab*0/, iy/0/
      if (idum.le.0) then
         idum=max(-idum,1)
         idum2=idum
         do j=ntab+8,1,-1
            k=idum/iq1
            idum=ia1*(idum-k*iq1)-k*ir1
            if (idum.lt.0) idum=idum+im1
            if (j.le.ntab) iv(j)=idum
         enddo
         iy=iv(1)
      endif 
      k=idum/iq1
      idum=ia1*(idum-k*iq1)-k*ir1
      if (idum.lt.0) idum=idum+im1
      k=idum2/iq2
      idum2=ia2*(idum2-k*iq2)-k*ir2
      if (idum2.lt.0) idum2=idum2+im2
      j=1+iy/ndiv
      iy=iv(j)-idum2
      iv(j)=idum
      if (iy.lt.1) iy=iy+imm1
      ran2=min(am*iy,rnmx)
      return
      end function

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c   MAPPINGS V.  An Astrophysical Plasma Modelling Code.
c
c   copyright 1979-2012
c
c       Version v5.1.13
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c     Subroutine to get the file header buisness out where
c     it can be worked on, and/or modified for other headers.
c
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
      subroutine p7fheader (newfil, banfil, fnam, filna, filnb, filnc,
     &filnd, filnt, filn, filnem, filin, luop, lupf, lusl, lups,
     &lusp, lunt, luions, luemiss, lulin, dhn, fin)
c
      include 'cblocks.inc'
c
c           Variables
c
      real*8 dhn,fin
      real*8 dht,zi(mxelem)
      integer*4 i, ie, j, at, io
      integer*4 idx, plen, nt, nl, itr
      integer*4 luop,lups,lupf,lusl,lusp,lunt,lulin
      integer*4 luions(4), luemiss(4)
      character fn*32
      character abundtitle*24
      character pfx*16,sfx*4
      character newfil*32, filna*32, banfil*32, fnam*32, filnb*32
      character filin*32
      character filnc*32, filnd*32, filnt*32, filn(4)*32, filnem(4)*32
c
      real*8 densnum
c
      dht=densnum(dhn)
c
      fn=' '
      pfx='photn'
      sfx='ph7'
      call newfile (pfx, 5, sfx, 3, fn)
      filna=fn(1:13)
      fnam=filna
c
c
      fn=' '
      pfx='phapn'
      sfx='ph7'
      call newfile (pfx, 5, sfx, 3, fn)
      filnb=fn(1:13)
c
      fn=' '
      pfx='phlss'
      sfx='ph7'
      call newfile (pfx, 5, sfx, 3, fn)
      filnc=fn(1:13)
c
      fn=' '
      pfx='phsem'
      sfx='ph7'
      call newfile (pfx, 5, sfx, 3, fn)
      filnt=fn(1:13)
c
      fn=' '
      pfx='emiss'
      sfx='csv'
      call newfile (pfx, 5, sfx, 3, fn)
      filin=fn(1:13)
c
      fn=' '
      pfx='spec'
      sfx='csv'
      call newfile (pfx, 4, sfx, 3, fn)
      filnd=fn(1:12)
c
      if (jall.eq.'YES') then
        fn=' '
        pfx='allion'
        sfx='ph6'
        call newfile (pfx, 6, sfx, 3, fn)
        newfil=fn(1:14)
      endif
c
      open (luop,file=filna,status='NEW')
      open (lupf,file=filnb,status='NEW')
      open (lups,file=filnc,status='NEW')
      open (lusp,file=filnd,status='NEW')
      open (lunt,file=filnt,status='NEW')
      if (jlin.eq.'YES') then
         open (lulin,file=filin,status='NEW')
      endif
      if (jall.eq.'YES') then
         open (lusl,file=newfil,status='NEW')
      endif
c
c     ***WRITES ON FILE INITIAL PARAMETERS
c
      write (luop,10) theversion,runname,banfil,fnam
      write (lupf,10) theversion,runname,banfil,fnam
      write (lups,10) theversion,runname,banfil,fnam
      write (lusp,10) theversion,runname,banfil,fnam
      write (lunt,10) theversion,runname,banfil,fnam
      if (jall.eq.'YES') then
        write (lusl,10) theversion,runname,banfil,fnam
      endif
      if (jlin.eq.'YES') then
        write (lulin,10) theversion,runname,banfil,fnam
      endif
   10 format(
     &'::::::::::::::::::::::::::::::::::::'
     &,'::::::::::::::::::::::::::::::::::::',/
     &          ' Photoionisation model P7, MAPPINGS V ',a8,/
     &'::::::::::::::::::::::::::::::::::::'
     &,'::::::::::::::::::::::::::::::::::::',/
     &' (Diffuse Field , Robust Integration, Radiation Pressure)',//
     &,' Run   :, ',a96/
     &,' Input :, ',a32/
     &,' Output:, ',a32/)
      write (lupf,20)
   20 format(' This file contains the plasma properties for ',
     &'each model step,'/
     &' and the pre-ionisation conditions used.'/ )
   30 format(' This file contains the ionisation fraction for ',
     &'all atomic elements as a function of distance,'/
     &' and the abundances used. See photn for model details.'/ )
      if (jall.eq.'YES') then
        write (lusl,30)
      endif
      write (lunt,40)
   40 format(' This file contains the nebula structure summary,'/
     &' and the strong line emissivities. See photn for model details/')
   45 format(' This file line emissivities (erg/cm^3/s/sr) for up to '
     & ,i2,' selected lines as a function of distance.',
     &          ' See photn file for model details/')
      if (jlin.eq.'YES') then
        write (lulin,45) mxmonlines
      endif
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c     abundances file header
c
c
      abundtitle=' Initial Abundances :'
      do i=1,atypes
        zi(i)=zion0(i)*deltazion(i)
      enddo
c
      call dispabundances (luop, zi, abundtitle)
c
      abundtitle=' Gas Phase Abundances :'
      call dispabundances (luop, zion, abundtitle)
c
      call wabund (lupf)
      call wabund (lups)
      call wabund (lusp)
      call wabund (lunt)
      if (jall.eq.'YES') then
        call wabund (lusl)
      endif
c
      close (lusp)
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
      if (jeq.eq.'E') then
        write (luop,50) dtau0
   50 format( ' Model Summary :',//
     &,t5,' Mode  : Thermal and Ionic Equilibrium ',/
     &,t5,' dTau  :',0pf8.5,/)
      elseif (jeq.eq.'F') then
        write (luop,60) telap,tlife,dtau0
   60 format( ' Model Summary :',//
     &,t5,' Mode        :  Non-Equilibrium + Finite Source Life ',/
     &,t5,' Age         :',1pg10.3,' sec',/
     &,t5,' Source Life :',1pg10.3,' sec',/
     &,t5,' dTau        :',0pf8.5,/)
      else
        write (luop,70) telap,dtau0
   70 format( ' Model Summary :',//
     &,t5,' Mode        :  Equilibrium + Source Switch off ',/
     &,t5,' Switch off  :',1pg10.3,' sec',/
     &,t5,' dTau        :',0pf8.5,/)
      endif
      if (usekappa) then
   80 format( ' Kappa Electron Distribution Enabled :',//
     & ,t5,' Electron Kappa : ',1pg11.4,/)
        write (luop,80) kappa
      endif
c
   90 format(/' Radiation Field and Parameters:',//,
     & t5, ' At estimated T_inner :',1pg10.3,' K'/
     & t5,' Rsou.',t20,' Remp.',t35,' Rmax',t50,' <DILU>'/
     & t5, 1pg10.3,t20,1pg10.3,t35,1pg10.3,t50,1pg10.3/
     & t5,' <Hdens>',t20,' <Ndens>',t35,' Fill Factor',/
     & t5, 1pg10.3,t20,1pg10.3,t35,0pf9.6,//
     & t5,' FQ tot',t20,' Log FQ',t35,' LQ tot',t50,' Log LQ',/
     & t5, 1pg10.3,t20,0pf8.3,t35,1pg10.3,t50,0pf8.3,/
     & t5,' Q(N) in',t20,' Log Q(N)',t35,' <Q(N)>',t50,' Log<Q(N)>',/
     & t5, 1pg10.3,t20,0pf8.3,t35,1pg10.3,t50,0pf8.3,/
     & t5,' Q(H) in',t20,' Log Q(H)',t35,' <Q(H)>',t50,' Log<Q(H)>',/
     & t5, 1pg10.3,t20,0pf8.3,t35,1pg10.3,t50,0pf8.3,/
     & t5,' U(N) in',t20,' Log U(N)',t35,' <U(N)>',t50,' Log<U(N)>',/
     & t5, 1pg10.3,t20,0pf8.3,t35,1pg10.3,t50,0pf8.3,/
     & t5,' U(H) in',t20,' Log U(H)',t35,' <U(H)>',t50,' Log<U(H)>',/
     & t5, 1pg10.3,t20,0pf8.3,t35,1pg10.3,t50,0pf8.3)
c
      write (luop,90) tinner,rstar,remp,rmax,wdpl,dhn,dht,fin,qht,
     &dlog10(qht),astar*qht,dlog10(astar*qht),qhdnin,dlog10(qhdnin),
     &qhdnav,dlog10(qhdnav),qhdhin,dlog10(qhdhin),qhdhav,dlog10(qhdhav),
     &unin,dlog10(unin),unav,dlog10(unav),uhin,dlog10(uhin),uhav,
     &dlog10(uhav)
c
c
  100 format(t6,'MOD',t10,'Temp.',t22,'Alpha',t31,'Turn-on',
     & t39,'Cut-off'/
     &,t6,a2,x,1pg10.3,x,0pf8.3,x,0pf8.3,x,0pf8.3,//
     &,t6,'Zstar',t16,'FQHI',t27,'FQHEI',t38,'FQHEII'/
     &,t6,0pf8.4,1x,3(1pg10.3,x),/)
      write (luop,100) iso,teff,alnth,turn,cut,zstar,qhi,qhei,qheii
c
      if (turbheatmode.eq.1) then
  110 format(/,'  Micro-Turbulent Dissipation Enabled, Mach = ',1pg10.3)
        write (*,110) admach
      endif
c
      if (grainmode.eq.1) then
        write (luop,140) galpha,amin(1),amax(1),amin(2),amax(2),
     &   graindens(1),graindens(2),bgrain,yinf
c
  120  format(  t5,' Projected dust area/H atom (graphite,silicate):',/
     &         ,t5,'  ',1pg12.4,1x,' (cm^2)',1x,1pg12.4,1x,' (cm^2)' )
        write (luop,120) siggrain(1),siggrain(2)
c
  130  format(  t5,' Composition Mass Ratios:  '/
     &           ,t5,'    Gas/H     :',1pg12.4/
     &           ,t5,'    Dust/Gas  :',1pg12.4/
     &           ,t5,'    PAH/Gas   :',1pg12.4/)
c
        write (luop,130) grainghr,graindgr,grainpgr
c
      endif
c
  140 format(   ' Dust Parameters :',//
     &         ,t5,' Alpha :',1pg10.3,/
     &         ,t5,' Graphite min:       max:',/
     &         ,t5,3x,1pg10.3,x,1pg10.3,/
     &         ,t5,' Silicate min:       max:',/
     &         ,t5,3x,1pg10.3,x,1pg10.3,/
     &         ,t5,' Graph dens  Sil dens',/
     &         ,t5,3x,1pg10.3,x,1pg10.3,/
     &         ,t5,' Bgrain      Yinf ',/
     &         ,t5,3x,1pg10.3,x,1pg10.3)
c
  150 format(   /,' Model Parameters:',//
     & ,t6,'Jden',t12,'Jgeo',t18,'Jend',t23,'Ielen',t29,
     &          'Jpoen',t38,'Fren',/
     & ,t6,3(a4,2x),2(i2,4x),0pf6.4,//
     & ,t6,'Tend',t12,'DIend',t22,'TAUen',t31,'Jeq',t38,'Teini',/
     & ,t5,0pf6.0,2(1pg10.3),x,a4,x,0pf7.1,//,
     &          '::::::::::::::::::::::::::::::::::::',
     &          '::::::::::::::::::::::::::::::::::::',/)
      write (luop,150) jden,jgeo,jend,ielen,jpoen,fren,tend,diend,tauen,
     &jeq,tm00
c
  160 format(/' Description of each space step:')
      write (luop,160)
c
      if ((grainmode.eq.1).and.(pahmode.eq.1)) then
        if (jgeo.eq.'S') then
  170 format(/,t3,'#',t8,'<Te>',t18,'<DLOS>',t29,'dChi',
     & t40,'dTau',t50,'<R>',t64,'Delta R',t78,'dr',t93,'nH',
     & t107,'ne',t120,'nt',t134,'F_HI',t148,'F_HII',t162,'Log<P/k>'
     & t176,'Log<Q(H)>',t190,'Log<U(H)>',t204,'Log<Q(N)>',
     & t218,'Log<U(N)',t232,'Hab.P.',t246,'QPAH/ISRF')
          write (luop,170)
        else
  180 format(/,t3,'#',t8,'<Te>',t18,'<DLOS>',t29,'dChi',
     & t40,'dTau',t50,'<X>',t64,'Delta X',t78,'dx',t93,'nH',
     & t107,'ne',t120,'nt',t134,'F_HI',t148,'F_HII',t162,'Log<P/k>'
     & t176,'Log<Q(H)>',t190,'Log<U(H)>',t204,'Log<Q(N)>',
     & t218,'Log<U(N)',t232,'Hab.P.',t246,'QPAH/ISRF')
          write (luop,180)
        endif
      else
        if (jgeo.eq.'S') then
  190 format(/,t3,'#',t8,'<Te>',t18,'<DLOS>',t29,'dChi',
     & t40,'dTau',t50,'<R>',t64,'Delta R',t78,'dr',t93,'nH',
     & t107,'ne',t120,'nt',t134,'F_HI',t148,'F_HII',t162,'Log<P/k>'
     & t176,'Log<Q(H)>',t190,'Log<U(H)>',t204,'Log<Q(N)>',
     & t218,'Log<U(N)')
          write (luop,190)
        else
  200 format(/,t3,'#',t8,'<Te>',t18,'<DLOS>',t29,'dChi',
     & t40,'dTau',t50,'<X>',t64,'Delta X',t78,'dx',t93,'nH',
     & t107,'ne',t120,'nt',t134,'F_HI',t148,'F_HII',t162,'Log<P/k>'
     & t176,'Log<Q(H)>',t190,'Log<U(H)>',t204,'Log<Q(N)>',
     & t218,'Log<U(N)')
          write (luop,200)
        endif
      endif
c
      if (jeq.eq.'F') write (luop,210)
  210 format(t73,'Fthick',t82,'Treach',t91,'DTCO',t100,'Dton',t109,
     &'DToff')
      if (jeq.eq.'P') write (luop,220)
  220 format(t100,'RECscal',t109,'DToff')
      close (luop)
      write (*,230) fnam
  230 format(/' Output in file : ',a/)
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c     phsem file header
c
c     contains structure summary and emissivity of strong lines
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
      if (jgeo.eq.'S') then
  240  format(/,'#,','[2]Delta R,','[3]dr,','[4]<R>,',
     &          '[5]<Te>,','[6]nH,','[7]ne,','[8]nt,',
     &          '[9]F_HI,','[10]F_HII,',
     &          '[11]HBeta,','[12]5007,'
     &          '[13]4363,','[14]3727+,','[15]6300,',
     &          '[16]6584,','[17]6725+','[18]HAlpha')
        write (lunt,240)
      else
  250  format(/,'#,','[2]Delta X,','[3]dx,','[4]<X>,',
     &          '[5]<Te>,','[6]nH,','[7]ne,','[8]nt,',
     &          '[9]F_HI,','[10]F_HII,',
     &          '[11]HBeta,','[12]5007,'
     &          '[13]4363,','[14]3727+,','[15]6300,',
     &          '[16]6584,','[17]6725+','[18]HAlpha')
        write (lunt,250)
      endif
c
      close (lunt)
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c     phapn file header
c
c     new format: contains plasma properties in line, for
c     easy plotting later.
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
  260 format(' Dist.',t14,'  Te',t28,'  de',t42,'  dh',t56,
     &'  en',t70,'  FHI',t84,'  Density',t98,'  Pressure',t112,
     &'  Flow',t126,'  Ram Press.',t140,'  Sound Spd',t154,
     &'  Slab depth',t168,'  Alfen Spd.',t182,
     &'  Mag. Field',t196,'  Grain Pot.')
      write (lupf,260)
c
      close (lupf)
c
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c     phlss file header
c
c     contains plasma cooling/heating in line, for
c     easy plotting later.
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
  270 format(' Dist.',t14,'  Te',t28,'  de',t42,'  dh',t56,'  en',t70,
     &     '  Dloss',t84,'  Eloss',t98,'  Egain',t112,'  Coll. H',t126,
     &     '  Chrg. Ex.',t140,'  Reson',t154,
     &     '  Xreson',t168,'  Inter/fine',t182,'  He int-forb',t196,
     &     '  Forbid',t210,'  Fe II',t224,'  2Photon',t238,
     &     '  Compton',t252,'  Free-Free',t266,'  Coll. Ion',t280,
     &     '  Photo.',t294,'  Recomb.',t308,'  Cosmic',t322,
     &     '  GrainsH-C',t336,'  G.Heat',t350,'  G.Cool',t364,
     &     '  PAHs')
      write (lups,270)
c
      close (lups)
c
      if (jall.eq.'YES') close (lusl)
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c     monitor ions
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
      if (jiel.eq.'YES') then
c
c     write ion balance files if requested
c
        do i=1,ieln
c
          if (i.eq.1) ie=iel1
          if (i.eq.2) ie=iel2
          if (i.eq.3) ie=iel3
          if (i.eq.4) ie=iel4
          fn=' '
          pfx=elem(ie)
          sfx='csv'
          call newfile (pfx, elem_len(ie), sfx, 3, fn)
          filn(i)=fn
        enddo
c
        do i=1,ieln
          open (luions(i),file=filn(i),status='NEW')
        enddo
c
c
        do i=1,ieln
c
          write (luions(i),'(" Run     : ",a96)') runname
c
  280  format(
     &          ' [ 1] <R>    , [ 2] DeltaR , [ 3] dR     , [ 4] <T>    , ',
     & 31(' [',i2,'] ', a6,', '))
  290  format(
     &          ' [ 1] <X>    , [ 2] DeltaX , [ 3] dX     , [ 4] <T>    , ',
     & 31(' [',i2,'] ', a6,', '))
          if (i.eq.1) then
            write (luions(i),'(" Element : ",a2)') elem(iel1)
            if (jgeo.eq.'S') then
              write (luions(i),280) (j+4,rom(j),j=1,maxion(iel1))
            else
              write (luions(i),290) (j+4,rom(j),j=1,maxion(iel1))
            endif
          endif
          if (i.eq.2) then
            write (luions(i),'(" Element: ",a2)') elem(iel2)
            if (jgeo.eq.'S') then
              write (luions(i),280) (j+4,rom(j),j=1,maxion(iel2))
            else
              write (luions(i),290) (j+4,rom(j),j=1,maxion(iel2))
            endif
          endif
          if (i.eq.3) then
            write (luions(i),'(" Element: ",a2)') elem(iel3)
            if (jgeo.eq.'S') then
              write (luions(i),280) (j+4,rom(j),j=1,maxion(iel3))
            else
              write (luions(i),290) (j+4,rom(j),j=1,maxion(iel3))
            endif
          endif
          if (i.eq.4) then
            write (luions(i),'(" Element: ",a2)') elem(iel4)
            if (jgeo.eq.'S') then
              write (luions(i),280) (j+4,rom(j),j=1,maxion(iel4))
            else
              write (luions(i),290) (j+4,rom(j),j=1,maxion(iel4))
            endif
          endif
c
        enddo
c
        do i=1,ieln
          close (luions(i))
        enddo
c
      endif
c
      if (jiem.eq.'YES') then
c
c     write ion emission files if requested
c
        do i=1,ieln
c
          idx=iemidx(i)
c
          at=fmatom(idx)
          io=fmion(idx)
          nt=nfmtrans(idx)
          nl=fmnl(idx)
          fn=' '
          pfx(1:16)='_'
          pfx(1:elem_len(at))=elem(at)
          plen=elem_len(at)+1
          pfx(plen:plen)='_'
          plen=plen+1
          pfx(plen:plen+rom_len(io))=rom(io)
          plen=plen+rom_len(io)+1
          pfx(plen:plen)='_'
          sfx='csv'
          call newfile (pfx, plen, sfx, 3, fn)
          filnem(i)=fn
c
          open (luemiss(i),file=filnem(i),status='NEW')
c
          write (luemiss(i),'("Run: ",a96)') runname
          write (luemiss(i),*) 'Emissivity (erg/cm^3/s/sr) for Ion: ',
     &     elem(at),rom(io)
  300     format(
     &          ' Step [1], <X> [2], DeltaX [3], dX [4], <T> [5], <HB> [6]',
     & 40(',',1pg12.6,'[',i2,']'))
          write (luemiss(i),300) (1.0d8*fmlam(itr,idx),itr+6,itr=1,nt)
c
          close (luemiss(i))
        enddo
c
      endif
c
      if (jlin.eq.'YES') then
 440    format(/,'     ,             ,             ,',
     &   '             ,             ,             ',
     &   16(',    ',a2,a6,'    '))
       write (lulin,440)(elem(emlinlistatom(itr)),rom(emlinlistion(itr))
     &                 ,itr=1,njlines)
 450   format( ' #[1],   Delta X[2],        dx[3],',
     &          '       <X>[4],       <T>[5],     HBeta[6]',
     &   16(',',f12.3,'[',i2,']'))
        write(lulin,450) (emlinlist(itr),itr+6,itr=1,njlines)
        close (lulin)
      endif
c
      return
c
      end
