cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c     MAPPINGS V include file used in montph7 (Monte-Carlo mode)
c     
c     Defined grid size: nx, ny, nz                           
c     
c     To those who change the grid size, please change 
c     nx,ny,nz here, because Fortran77 doesn't have dynamic 
c     memory allocation
c
c     Contact Yifei Jin (Yifei.Jin@anu.edu.au) for details
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c   Grid size parameter
c     nx, ny, nz:    number of cell in x/y/z axis 
c                    (number of cell wall: nx+1, ny+1, nz+1)
c
c     dx, dy, dz:    cell length 
c                    (in unit of centi-meter)
c
c     xst, yst, zst: cell wall start point 
c                    corresponding to (0,0,0) in Cartesian
c                    (in unit of centi-meter)
c
c     xctr, yctr, zctr: neublar center (in unit of centi-meter)
c                    
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c

      parameter (safelmt=100)
c
      parameter (nx = 13)
      parameter (ny = 13)
      parameter (nz = 13)
c
      parameter (dx = 3.1d18)
      parameter (dy = 3.1d18)
      parameter (dz = 3.1d18)      
c
      parameter (xctr = 0)
      parameter (yctr = 0)
      parameter (zctr = 0)  
c
      parameter (xst = -2.01500d19-xctr)
      parameter (yst = -2.01500d19-xctr)
      parameter (zst = -2.01500d19-xctr)      

c

