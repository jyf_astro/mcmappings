cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c   MAPPINGS V.  An Astrophysical Plasma Modelling Code.
c
c   copyright 1979-2012
c
c       Version v5.1.13
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c*******COMPUTES Monte-Carlo random number criteria*******
c
c       INPUT : temperature: t
c               electron denstiy: de
c               hydrogen denstiy: dh
c       OUTPUT: continuum energy distribution: recPDF (G.V.)
c               fraction of energy in non-ionizing lines: totLine (G.V.)
c     
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine emPDF (t, de, dh)
c
      include 'cblocks.inc'
      include 'const.inc'
c
c           Variables
c
      real*8 t, de, dh, telc
      real*8 xp(mxion, mxelem)
      real*8 energ,rkt
      real*8 wid
c
c      integer*4 ie,ii2,jn
      integer i,j,inl,nz
      integer line,series,atom,ion,trans
c
      telc=dmax1(t,1.d-2)
      rkt=rkb*telc
c
      do j=1,atypes
        do i=1,maxion(j)-1
          xp(i,j)=1.0d0
        enddo
      enddo
c
c     ***FIND LINES BRIGHTNESSES
c
      call cool (t, de, dh)
c
      jcon='YES'
c
      do inl=1,infph
        emidifcont(inl)=0.d0
      enddo
c
      call freebound (t, de, dh)
      call freefree (t, de, dh)
      call twophoton (t, de, dh)
c
      do inl=1,infph
c
         wid=(photev(inl+1)-photev(inl))*evplk
         emidifcont(inl)=(ffph(inl)+fbph(inl)+p2ph(inl))*cphote(inl)*wid
         emidif(inl)=0. !emidifcont(inl)
c
      enddo
cc
c      endif
cc
c  If dust and IR included add dust continuum to continuum emission
c
ccc      if (grainmode.eq.1) then
ccc        if (irmode.ne.0) then
ccc          do inl=1,infph
ccc            emidifcont(inl)=emidifcont(inl)+irphot(inl)
ccc            emidif(inl)=emidifcont(inl)
ccc          enddo
ccc        endif
ccc        if ((pahmode.eq.1).and.(pahactive.eq.1).and.(irmode.ne.0)) then
ccc          do inl=1,infph
ccc            emidifcont(inl)=emidifcont(inl)
ccc     &                     +paheng*pahflux(inl)*pahfrac*dh
ccc            emidif(inl)=emidifcont(inl)
ccc          enddo
ccc        endif
ccc      endif
c
c     Now add weak lines directly to vector emidif...but not
c     emidifcont
c
c    Multi-level atoms
c
      do ion=1,nfmions
        do trans=1,nfmtrans(ion)
          j=fmbin(trans,ion)
          if ((j.ne.0).and.(fmbri(trans,ion).gt.epsilon)) then
c            energ=fmeij(trans,ion)
c            wid=evplk*(photev(j+1)-photev(j))
             if (photev(j).lt.iph) then
                emidif(j)=emidif(j)+fmbri(trans,ion)
             else
                emidifcont(j)=emidifcont(j)+fmbri(trans,ion)
             endif   
          endif
        enddo
      enddo
cc
cc    Fe II-VII lines
c
      do ion=1,nfeions
        do trans=1,nfetrans(ion)
          j=febin(trans,ion)
          if ((j.ne.0).and.(febri(trans,ion).gt.epsilon)) then
c            energ=feeij(trans,ion)
c            wid=evplk*(photev(j+1)-photev(j))
             if (photev(j).lt.iph) then
                emidif(j)=emidif(j)+febri(trans,ion)
             else
                emidifcont(j)=emidifcont(j)+febri(trans,ion)
             endif   
          endif
        enddo
      enddo
cc
c
c    Three level atoms
c
      do ion=1,nf3ions
        do trans=1,nf3trans
          j=f3bin(trans,ion)
          if ((j.ne.0).and.(f3bri(trans,ion).gt.epsilon)) then
c            energ=(plk*cls)/f3lam(trans,ion)
c            wid=evplk*(photev(j+1)-photev(j))
             if (photev(j).lt.iph) then  
                emidif(j)=emidif(j)+f3bri(trans,ion)
             else
                emidifcont(j)=emidifcont(j)+f3bri(trans,ion)
             endif
          endif
        enddo
      enddo
c
c     Add old intercombination lines to vector EMDIF.
c
      do i=1,mlines
        j=lcbin(i)
        if ((j.ne.0).and.(fsbri(i).gt.epsilon)) then
c          energ=e12fs(i)
c          wid=evplk*(photev(j+1)-photev(j))
           if (photev(j).lt.iph) then            
              emidif(j)=emidif(j)+fsbri(i)
           else
              emidifcont(j)=emidifcont(j)+fsbri(i)
           endif      
        endif
      enddo
c
C     do i=1,xilines
C       j=xibin(i)
C       if ((j.gt.0).and.(xibri(i).gt.epsilon)) then
C         energ=ev*xiejk(i)
C         wid=evplk*(photev(j+1)-photev(j))
C         emidif(j)=emidif(j)+(xibri(i)/(wid*energ))
C       endif
C     enddo
c
C     do i=1,xhelines
C       j=xhebin(i)
C       if ((j.gt.0).and.(xhebri(i).gt.epsilon)) then
C         energ=ev*xiejk(i)
C         wid=evplk*(photev(j+1)-photev(j))
C         emidif(j)=emidif(j)+(xhebri(i)/(wid*energ))
C       endif
C     enddo
c
c old intercombination lines
c
      do i=1,nlines
        j=lrbin(i)
        if ((j.ne.0).and.(rbri(i).gt.epsilon)) then
c          energ=e12r(i)
c          wid=evplk*(photev(j+1)-photev(j))
           if (photev(j).lt.iph) then            
              emidif(j)=emidif(j)+rbri(i)
           else
              emidifcont(j)=emidifcont(j)+rbri(i)
           endif      
        endif
      enddo
c Original He I lines, just the first three
      do i=1,3
        j=heibin(i)
        if ((j.ne.0).and.(heibri(i).gt.epsilon)) then
c          energ=ev*(lmev/(heilam(i)*1.d8))
c          wid=evplk*(photev(j+1)-photev(j))
           if (photev(j).lt.iph) then  
              emidif(j)=emidif(j)+heibri(i)
           else
              emidifcont(j)=emidifcont(j)+heibri(i)
           endif   
        endif
      enddo
c
c New He I singlet lines
c
      do i=1,nheislines
        j=heisbin(i)
        if ((j.ne.0).and.(heisbri(i).gt.epsilon)) then
c          energ=heiseij(i)
c          wid=evplk*(photev(j+1)-photev(j))
           if (photev(j).lt.iph) then
              emidif(j)=emidif(j)+heisbri(i)
           else
              emidifcont(j)=emidifcont(j)+heisbri(i)
           endif
        endif
      enddo
c
c New He I triplet lines
c
      do i=1,nheitlines
        j=heitbin(i)
        if ((j.ne.0).and.(heitbri(i).gt.epsilon)) then
c          energ=heiteij(i)
c          wid=evplk*(photev(j+1)-photev(j))
           if (photev(j).lt.iph) then 
              emidif(j)=emidif(j)+heitbri(i)
           else
              emidifcont(j)=emidifcont(j)+heitbri(i)
           endif
        endif
      enddo
c
c heavy element recomb lines
c
      do i=1,nrccii
        j=rccii_bin(i)
        if ((j.ne.0).and.(rccii_bbri(i).gt.epsilon)) then
c          energ=rccii_eij(i)
c          wid=evplk*(photev(j+1)-photev(j))
           if (photev(j).lt.iph) then
              emidif(j)=emidif(j)+rccii_bbri(i)
           else
              emidifcont(j)=emidifcont(j)+rccii_bbri(i)
           endif
        endif
      enddo
c
      do i=1,nrcnii
        j=rcnii_bin(i)
        if ((j.ne.0).and.(rcnii_bbri(i).gt.epsilon)) then
c          energ=rcnii_eij(i)
c          wid=evplk*(photev(j+1)-photev(j))
           if (photev(j).lt.iph) then
              emidif(j)=emidif(j)+rcnii_bbri(i)
           else
              emidifcont(j)=emidifcont(j)+rcnii_bbri(i)
           endif
        endif
      enddo
c
      do i=1,nrcoi_q
        j=rcoi_qbin(i)
        if ((j.ne.0).and.(rcoi_qbbri(i).gt.epsilon)) then
c          energ=rcoi_qeij(i)
c          wid=evplk*(photev(j+1)-photev(j))
           if (photev(j).lt.iph) then       
              emidif(j)=emidif(j)+rcoi_qbbri(i)
           else
              emidifcont(j)=emidifcont(j)+rcoi_qbbri(i)
           endif
        endif
      enddo
c
      do i=1,nrcoi_t
        j=rcoi_tbin(i)
        if ((j.ne.0).and.(rcoi_tbbri(i).gt.epsilon)) then
c          energ=rcoi_teij(i)
c          wid=evplk*(photev(j+1)-photev(j))
           if (photev(j).lt.iph) then
              emidif(j)=emidif(j)+rcoi_tbbri(i)
           else
              emidifcont(j)=emidifcont(j)+rcoi_tbbri(i)
           endif
        endif
      enddo
c
      do i=1,nrcoii
        j=rcoii_bin(i)
        if ((j.ne.0).and.(rcoii_bbri(i).gt.epsilon)) then
c          energ=rcoii_eij(i)
c          wid=evplk*(photev(j+1)-photev(j))
           if (photev(j).lt.iph) then
              emidif(j)=emidif(j)+rcoii_bbri(i)
           else
              emidifcont(j)=emidifcont(j)+rcoii_bbri(i)
           endif
        endif
      enddo
c
      do i=1,nrcneii
        j=rcneii_bin(i)
        if ((j.ne.0).and.(rcneii_bbri(i).gt.epsilon)) then
c          energ=rcneii_eij(i)
c          wid=evplk*(photev(j+1)-photev(j))
           if (photev(j).lt.iph) then
              emidif(j)=emidif(j)+rcneii_bbri(i)
           else
              emidifcont(j)=emidifcont(j)+rcneii_bbri(i)
           endif
        endif
      enddo
c
c     adds xr3lines to xr3lines_emilin(1,line)
c
      do line=1,nxr3lines
        xr3lines_emilin(1,line)=0.d0
        if (xr3lines_bri(line).gt.epsilon) then
          j=xr3lines_bin(line)
          if (j.ne.0) then
c          energ=xr3lines_egij(line)
c          wid=evplk*(photev(j+1)-photev(j))
             if (photev(j).lt.iph) then
                xr3lines_emilin(1,line)=xr3lines_bri(line)
                emidif(j)=emidif(j)+xr3lines_bri(line)
             else
                emidifcont(j)=emidifcont(j)+xr3lines_bri(line)
             endif   
          endif
        endif
      enddo
c
c     adds xrllines to xrllines_emilin(1,line)
c
      do line=1,nxrllines
        xrllines_emilin(1,line)=0.d0
        if (xrllines_bri(line).gt.epsilon) then
          j=xrllines_bin(line)
          if (j.ne.0) then
c          energ=xrllines_egij(line)
c          wid=evplk*(photev(j+1)-photev(j))
             if (photev(j).lt.iph) then
                xrllines_emilin(1,line)=xrllines_bri(line)
                emidif(j)=emidif(j)+xrllines_bri(line)
             else
                emidifcont(j)=emidifcont(j)+xrllines_bri(line)
             endif  
           endif
        endif
      enddo
c
c     adds xlines to emilin(1,line)
c
C     do line=1,xlines
C       emilin(1,line)=0.d0
C       if (xrbri(line).gt.epsilon) then
C         j=xbin(line)
C         if (j.ne.0) then
C         energ=ev*xejk(line)
C         wid=evplk*(photev(j+1)-photev(j))
C         emilin(1,line)=xrbri(line)/(wid*energ)
C       endif
C       endif
C     enddo
c
c
c     adds hydrogen and helium lines to hydlin(1,line,series) and
c      hellin(1,line,series)
c
      do series=1,nhseries
        do line=1,nhlines
          hydlin(1,line,series)=0.d0
          if (hydrobri(line,series).gt.epsilon) then
             j=hbin(line,series)
             if (j.ne.0) then
c            energ=ev*lmev/hlambda(line,series)
c            wid=evplk*(photev(j+1)-photev(j))
                if (photev(j).lt.iph) then
                   hydlin(1,line,series)=hydrobri(line,series)
                   emidif(j)=emidif(j)+hydrobri(line,series)
                else 
                   emidifcont(j)=emidifcont(j)+hydrobri(line,series)
                endif
             endif
          endif
        enddo
      enddo
c
      do series=1,nheseries
        do line=1,nhelines
          hellin(1,line,series)=0.d0
          if (helibri(line,series).gt.epsilon) then
             j=hebin(line,series)
             if (j.ne.0) then
c            energ=ev*lmev/helambda(line,series)
c            wid=evplk*(photev(j+1)-photev(j))
                if (photev(j).lt.iph) then
                   hellin(1,line,series)=helibri(line,series)
                   emidif(j)=emidif(j)+helibri(line,series)
                else
                   emidifcont(j)=emidifcont(j)+helibri(line,series)                
                endif   
             endif
          endif
        enddo
      enddo
c
c     Adds heavy hydrogenic series
c
      if (atypes.gt.3) then
        do atom=3,atypes
c
          nz=mapz(atom)
c
          do series=1,nxhseries
            do line=1,nxhlines
              xhydlin(1,line,series,atom)=0.d0
              if (xhydrobri(line,series,atom).gt.epsilon) then
                 j=xhbin(line,series,atom)
                 if (j.ne.0) then
c            energ = 0.5d0*(ev*(photev(j+1)+photev(j)))
c                energ=ev*lmev/xhlambda(line,series,atom)
c                wid=evplk*(photev(j+1)-photev(j))
                    if (photev(j).lt.iph) then
                       xhydlin(1,line,series,atom)=xhydrobri(line,series,atom)
                       emidif(j)=emidif(j)+xhydrobri(line,series,atom)
                    else
                       emidifcont(j)=emidifcont(j)+xhydrobri(line,series,atom)                    
                    endif   
                 endif
              endif
            enddo
          enddo
c      endif
        enddo
      endif

      normal = 0.
      totLine = 0.
      do inl=1,infph
         normal=normal + emidifcont(inf)
         totLine=totLine + emidif(inf)
      enddo

      do inl=1,infph
         recPDF(inl)=0.
         recPDF(inl)=emidifcont(inf)/normal
         if ((dabs(recPDF(inl)-1.).le.1.d12).or.(recPDF(inl).ge.1)) recPDF(inl) = 1.
      enddo

      totLine = totLine/(totLine+normal)
c
      return
c
      end
